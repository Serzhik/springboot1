package com.spring.springboot1.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "groupp")
public class Group {
    @Id
    private int gpId;
    @Column
    private String groupName;

    @Override
    public String toString() {
        return "Group{" + "gpId=" + gpId + ", groupName='" + groupName + '\'' + '}';
    }

    public int getgpId() {
        return gpId;
    }

    public void setgpId(int gpId) {
        this.gpId = gpId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
