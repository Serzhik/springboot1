package com.spring.springboot1.repository;

import com.spring.springboot1.entity.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface GroupRepository extends CrudRepository<Group, Long>{

}
