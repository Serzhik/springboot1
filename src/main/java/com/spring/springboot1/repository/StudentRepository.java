package com.spring.springboot1.repository;

import com.spring.springboot1.entity.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface StudentRepository extends CrudRepository<Student, Long>{

    @Query(value = "select * from student where gpid = 2", nativeQuery = true)
    Student getStudent();

    List<Student> findAllByGroupId(long group_id);

    List<Student> findAll();

}
